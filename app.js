const express = require("express");
const path = require("path");
const fs = require("fs");
// const yaml = require("js-yaml");

// GLOBAL VARIABLES

// SWAGGER
// let swagger = yaml.load(fs.readFileSync("./openapi.yaml", "utf8"));
// const apiPostFile = swagger.paths["/api/files"].post.responses;
// const apiGetFile = swagger.paths["/api/files"].get.responses;
// const apiGetFileName = swagger.paths["/api/files/{filename}"].get.responses;
// const port = 8080;

// APP
const app = express();
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// VIEWS
// app.set("views", "./views");
// app.set("view engine", "pug");
app.get("/", (req, res) => {
  // res.render("index", { data: swagger });
  res.send({ message: "Home Page" });
});

// CREATE FILES
app.post("/api/files", (req, res) => {
  const { filename, content } = req.body;
  const regex = /(json|js|yaml|txt|log|xml)$/;
  const pathToFolder = `${__dirname}/createdFiles/`;
  const pathToFile = pathToFolder + `${filename}`;

  if (!filename || !content) {
    const message = { message: "Bad request" };
    console.log(JSON.stringify(message));
    return res.status(400).send(message);
  }
  if (filename && !regex.test(filename)) {
    const message = {
      message: "Please specify 'file' parameter",
    };
    console.log(req.body);
    console.log(JSON.stringify(message));
    return res.status(400).send(message);
  }

  if (!fs.existsSync(pathToFolder)) {
    fs.mkdirSync(pathToFolder);
  }

  fs.writeFile(pathToFile, content, (err) => {
    let message = {};

    if (err) {
      message = { message: "Server error" };
      console.log(JSON.stringify(message));
      return res.status(500).send(message);
    } else {
      message = {
        message: "File created successfully",
      };
      console.log(JSON.stringify(message));
      return res.status(200).send(message);
    }
  });
});

// READ FILES
app.get("/api/files", (req, res) => {
  const pathToFolder = `${__dirname}/createdFiles/`;
  fs.readdir(pathToFolder, (err, items) => {
    let message = {};

    if (err) {
      message = { message: "Server error" };
      console.log(JSON.stringify(message));
      return res.status(500).send(message);
    }

    if (Object.keys(req.query).length !== 0) {
      message = {
        message: "Client error",
      };
      console.log(JSON.stringify(message));
      return res.status(400).send(message);
    }

    message = { message: "Success", files: items };
    console.log(JSON.stringify(message));
    return res.status(200).send(message);
  });
});

app.get("/api/files/:filename", (req, res) => {
  const pathToFolder = `${__dirname}/createdFiles/`;
  const filename = req.params.filename;
  const pathToFile = pathToFolder + `${filename}`;

  fs.readFile(pathToFile, (err, data) => {
    let message = {};

    if (err) {
      if (err.code === "ENOENT") {
        message = {
          message: `No file with '${filename}' filename found`,
        };
        console.log(JSON.stringify(message));
        res.status(400).send(message);
      } else {
        message = { message: "Server error" };
        console.log(JSON.stringify(message));
        return res.status(500).send(message);
      }
    } else {
      const stats = fs.statSync(pathToFile);
      const [ext] = filename.match(/([a-z]+)(?:[\?#]|$)/g);
      message = {
        message: "Success",
        filename: filename,
        content: data.toString(),
        extension: ext,
        uploadedDate: stats.birthtime,
      };
      console.log(JSON.stringify(message));
      return res.status(200).send(message);
    }
  });
});

app.listen(8080, () => {
  console.log(`Express running → PORT 8080`);
});
